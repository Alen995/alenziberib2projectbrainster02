<?php require_once "db.php";
session_start();
session_unset();
session_destroy();



if (isset($_GET['id'])) {
    $countryId = $_GET['id'];
    $countryData = $queryObj->countryTotal($countryId);
     $countryDataDaily = $queryObj->countryToday($countryId);

    if (isset($_GET['type']) && $_GET['type'] == 'month') {

        $data = $queryObj->countryMonth($countryId);

        $show = 'This Month';
    } else if (isset($_GET['type']) && $_GET['type'] == '3months') {

        $data = $queryObj->countryThreeMonths($countryId);

        $show = 'Last Three Months';
    } else {
        $data = $queryObj->countryToday($countryId);
$data=$data[0];
        $show = 'Today';
    }


    // echo "<pre>";
    // var_dump($countryData);
    // echo "</pre>";
} else {
    header('Location: index.php');
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once "parcels/headers.php" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

</head>

<body>
    <?php require_once "parcels/navbar.php" ?>


    <div class="container my-5">

        <div class="row">
            <div class="col-12 col-md-8 offset-0 offset-md-2 text-center  border-bottom">
                <h2 id="country_name"><?= $countryData[0]['name'] ?></h2>
            </div>
        </div>
        <div class="row mt-5 align-items-center">


            <div class="col-12 text-center my-1 myBtns">
                <a href="?id=<?=$countryId?>&type=daily" class="btn btn-outline-dark" id="today_id">Today</a>
                <a href="?id=<=$countryId?>&type=month" class="btn btn-outline-dark" id="month_id">This Month</a>
                <a href="?id=<=$countryId?>&type=3months" class="btn btn-outline-dark" id="months_id">Last three months </a>
                <p class="h4 mt-3"><?= $show ?></p>
            </div>

            <div class="col-12 col-md-2 text-center">
                <div class="status-box status-active ">
                    <i class=" fas fa-2x fas fa-viruses active-icon"></i>
                    <h4 class="">Confirmed </h4>
                    <p id="activeTotal_id" class="h3"><?= "{$data['confirmed']}" ?></p>
                </div>

            </div>
            <div class="col-12 col-md-2 text-center">
                <div class="status-box status-recovered">
                    <i class="fas fa-2x fas fa-heartbeat recovered-icon "></i>
                    <h4 class="">Recovered </h4>
                    <p id="recoveredTotal_id" class="h3"><?= $data['recovered'] ?></p>
                    </p>
                </div>

            </div>
            <div class="col-12 col-md-2 text-center">
                <div class="status-box status-death">
                    <i class="fas fa-2x fa-skull-crossbones death-icon"></i>
                    <h4 class="">Deaths </h4>
                    <p id="deathsTotal_id" class="h3"><?= $data['deaths'] ?></p>
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6 text-center">
                <canvas id="myChart"></canvas>
            </div>

        </div>

        <div class="row mb-5">
            <div class="col-12 offset-0 col-md-8 offset-2 ">

                <table id="myTable" class="display table-responsive ">
                    <thead>
                        <tr>
                            <th>id</th>

                   
                            <th> Confirmed</th>
                            <th>Recovered</th>
                            <th>Deaths</th>
                            <th>Date</th>

                        </tr>
                    </thead>
                    <tbody id=''>
                        <?php foreach ($countryDataDaily as $key => $date) { ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                            
                                <td> <?= $date['confirmed'] ?></td>
                                <td><?= $date['recovered'] ?></td>
                                <td><?= $date['deaths'] ?></td>
                                <th><?= $date['date'] ?></th>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            

        </div>
    </div>
    <div class="col-8 offset-0 offset-md-2 mt-5"> <canvas id="lineChart"></canvas></div>



    <?php require_once "parcels/scripts.php" ?>

    <script src="public/js/myCharts.js"></script>
    <script>
        <?php $dataResult = $queryObj->countryDataMonthly($countryId) ?>
        $(document).ready(function() {


            $('#myTable').DataTable();
            // 
            new Chart(document.getElementById("lineChart"), {
                type: 'line',
                data: {
                    labels: [
                        <?php
                        for ($i = 0; $i < count($dataResult); $i++) {
                            $tmp = (string)(date('M-Y', strtotime($dataResult[$i]['date'])));
                            echo  "'$tmp'" . ',';
                        }
                        ?>
                    ],
                    datasets: [{
                            data: [
                                <?php
                                for ($i = 0; $i < count($dataResult); $i++) {
                                    echo ($dataResult[$i]['ConfirmedCases']) . ',';
                                }
                                ?>
                            ],
                            borderColor: "#3E95CD",
                            fill: false,
                            label: 'Year based confirmed cases'
                        },
                        {
                            data: [
                                <?php
                                for ($i = 0; $i < count($dataResult); $i++) {
                                    echo ($dataResult[$i]['DeathCases']) . ',';
                                }
                                ?>
                            ],
                            borderColor: "rgb(217, 83, 79)",
                            fill: false,
                            label: 'Year based death cases'
                        },
                        {
                            data: [
                                <?php
                                for ($i = 0; $i < count($dataResult); $i++) {
                                    echo ($dataResult[$i]['RecoveredCases']) . ',';
                                }
                                ?>
                            ],
                            borderColor: "rgb(92, 184, 92)",
                            fill: false,
                            label: 'Year based recovered cases'
                        }
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Montly Based Statistics '
                    }
                }
            });
        new Chart(document.getElementById('myChart'), {
                type: 'bar',
                data: {
                    labels: ["Deaths", "Recovered", "Confirmed"],
                    datasets: [{
                        label: 'cases',
                        data: [<?= "{$data['deaths']},{$data['recovered']},{$data['confirmed']}" ?>],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.8)',
                            'rgba(54, 162, 235, 0.8)',
                            'rgba(255, 206, 86, 0.8)',

                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',

                        ],
                        borderWidth: 1
                    }]
                },

            });

        });
    </script>
</body>

</html>
