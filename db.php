<?php require "app/Database/Connection.php" ?>
<?php require "app/Database/Query.php" ?>
<?php require "app/Classes/Model.php" ?>
<?php require "app/Classes/Cases.php" ?>
<?php require "app/Classes/User.php" ?>

<?php

$totalCases = $queryObj->casesTotal();
$totalCasesToday = $queryObj->casesToday();
$totalCasesMonth = $queryObj->casesMonth();
$totalCasesThreeMonths = $queryObj->casesThreeMonths();
$allCountryTotals = $queryObj->allCountriesTotals();


// echo "<pre>";
// var_dump($totalCasesMonth);
// echo "</pre>";

?>