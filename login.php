<?php
require_once "db.php";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $user->checkUser($username, $password);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once "parcels/headers.php" ?>
</head>

<body>

    <?php require_once "parcels/navbar.php" ?>

    <div class="container mt-5">
     
        <div class="col-12 col-md-8 offset-0 offset-md-2">
        <?php if(isset($_GET['error'])){?>
          
          <div class='aler alert-danger text-center'>Invali Admin</div>
                  <?php } ?>
            <form action=""method="POST">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" name="username" id="username" aria-describedby="text">

                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password"name="password" class="form-control" id="exampleInputPassword1">
                </div>
               
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>








    <?php require_once "parcels/scripts.php" ?>
</body>

</html>