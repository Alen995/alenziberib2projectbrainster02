function createConfig() {
    return {
        type: 'line',
        data: {
            labels: ['2010-20-21', '2010-20-22', '2010-20-23'],
            datasets: [{
                label: ['Red', 'blue'],
                borderColor: "red",
                backgroundColor: "red",
                data: ['1', '2', '3'],
                fill: false,
            }, {
                label: 'Recovered',
                borderColor: 'green',
                backgroundColor: 'green',
                data: ['7', '3', '1'],
                fill: false,
            }, {
                label: 'Active',
                borderColor: 'blue',
                backgroundColor: 'blue',
                data: ['6', '12', '3'],
                fill: false,
            }, {
                label: 'Confirmed',
                borderColor: 'purple',
                backgroundColor: 'purple',
                data: ['10', '20', '13'],
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Line Chart'
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]

            }
        }
    }
}