<?php require_once "./db.php" ?>
<?php

session_start();
session_unset();
session_destroy();





if (isset($_GET['type']) && $_GET['type'] == 'month') {

    $data = $totalCasesMonth[0];
    $show = 'This Month';
} else if (isset($_GET['type']) && $_GET['type'] == '3months') {

    $data = $totalCasesThreeMonths[0];
    $show = 'Last Three Months';
} else {
    $data = $totalCasesToday;

    $show = 'Today';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once "parcels/headers.php" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

</head>

<body>
    <?php require_once "parcels/navbar.php" ?>

    <div class="container  ">

        <div class="row my-5 align-items-center">

            <div class="col-12  text-center my-4">
                <h2>Global Covid Status</h2>
            </div>
            <div class="col-12 text-center my-3 myBtns">
                <a href="?type=daily" class="btn btn-outline-dark" id="today_id">Today</a>
                <a href="?type=month" class="btn btn-outline-dark" id="month_id">This Month</a>
                <a href="?type=3months" class="btn btn-outline-dark" id="months_id">Last three months </a>
                <p class="h4 mt-3"><?= $show ?></p>
            </div>

            <div class="col-12 col-md-2 text-center">
                <div class="status-box status-active ">
                    <i class=" fas fa-2x fas fa-viruses active-icon"></i>
                    <h4 class="">Confirmed </h4>
                    <p id="activeTotal_id" class="h3"><?= "{$data['confirmed']}" ?></p>
                </div>

            </div>
            <div class="col-12 col-md-2 text-center">
                <div class="status-box status-recovered">
                    <i class="fas fa-2x fas fa-heartbeat recovered-icon "></i>
                    <h4 class="">Recovered </h4>
                    <p id="recoveredTotal_id" class="h3"><?= $data['recovered'] ?></p>
                    </p>
                </div>

            </div>
            <div class="col-12 col-md-2 text-center">
                <div class="status-box status-death">
                    <i class="fas fa-2x fa-skull-crossbones death-icon"></i>
                    <h4 class="">Deaths </h4>
                    <p id="deathsTotal_id" class="h3"><?= $data['deaths'] ?></p>
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6 text-center">
                <canvas id="myChart"></canvas>
            </div>

        </div>



        <div class="row my-5">
            <div class="col-12 col-md-8 offset-0 offset-md-2 text-center mb-5 border-bottom">
                <h2>Data for all Countries</h2>
            </div>
            <div class="col-12 col-md-8 offset-0 offset-md-2">
                <table id="myTable" class="display table-responsive">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>active</th>
                            <th> Confirmed</th>
                            <th>Recovered</th>
                            <th>Deaths</th>

                        </tr>
                    </thead>
                    <tbody id='tBody_id'>
                        <?php foreach ($allCountryTotals as $key => $country) { ?>
                            <tr>
                                <td><?= $key ?></td>
                                <td><a href="/showCountry.php?id=<?= $country['country_id'] ?>"><?= $country['name'] ?></a></td>
                                <td><?= $country['active'] ?></td>
                                <td> <?= $country['confirmed'] ?></td>
                                <td><?= $country['recovered'] ?></td>
                                <td><?= $country['deaths'] ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>


    </div>











    <?php require_once "parcels/scripts.php" ?>
    <script src='public/js/homescript.js'></script>


    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
            let ctx = document.getElementById("myChart");


            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Deaths", "Recovered", "Confirmed"],
                    datasets: [{
                        label: '# of Votes',
                        data: [<?= "{$data['deaths']},{$data['recovered']},{$data['confirmed']}" ?>],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.8)',
                            'rgba(54, 162, 235, 0.8)',
                            'rgba(255, 206, 86, 0.8)',

                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',

                        ],
                        borderWidth: 1
                    }, {

                    }]
                },

            });





        });
    </script>



</body>

</html>