<?php

namespace App\Database;


class Connection
{
    protected $pdo = null;

// connection info

    protected $host = 'localhost';
    protected $driver = 'mysql';
    protected $dbname = 'covidapptest';
    protected $username = 'root';
    protected $password = '';

    protected function connect()
    {
        $dns = "$this->driver:host=$this->host;dbname=$this->dbname";
        if ($this->pdo == NULL) {
            try {
                $this->pdo =new \PDO($dns, $this->username, $this->password);
            } catch (\PDOException $e) {
                return $e->getMessage();
            }
        }
    }
}
