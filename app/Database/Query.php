<?php

namespace App\Database;




use App\Database\Connection;

class Query extends Connection
{
    //------------------------------- za svet
    // vkupno  do denes
    public function casesTotal()
    {
        $this->connect();

        $sql = "SELECT cases.date ,SUM(cases.confirmed)as confirmed,SUM(cases.recovered)as recovered,SUM(cases.deaths)as deaths,SUM(cases.active)as active FROM cases JOIN countries ON cases.country_id=countries.id JOIN (SELECT country_id, max(date) as Date FROM cases GROUP BY country_id) as a ON cases.country_id = a.country_id and cases.date = a.date";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }
    //  vo svetot samo denes
    public function casesToday()
    {
        $this->connect();
        $sql = "SELECT SUM(cases.death_today)as deaths, SUM(cases.confirmed_today)as confirmed, SUM(cases.recovered_today)as recovered,MaxDate FROM cases INNER JOIN ( SELECT country_id, MAX(DATE) AS MaxDate FROM cases GROUP BY country_id ) tm ON cases.country_id = tm.country_id AND cases.date = tm.MaxDate WHERE cases.date=MaxDate";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }
    // kolku cases ima za sekoj mesec vo svet
    public function casesMonth()
    {
        $date = date('Y-m-d', strtotime("-1 days"));


        $sql = "SELECT SUM(confirmed_today)as confirmed ,
        SUM(recovered_today) as recovered,
        SUM(death_today)as deaths
         FROM cases WHERE date BETWEEN DATE_ADD('$date', INTERVAL - 1 MONTH) AND '$date'";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    // kolku cases ima za poslednite tri meseci 3 meseci
    public function casesThreeMonths()
    {
        $date = date('Y-m-d', strtotime("-1 days"));
        $date = date('Y-m-d');

        $sql = "SELECT SUM(confirmed_today)as confirmed ,
        SUM(recovered_today) as recovered,
        SUM(death_today)as deaths
         FROM cases WHERE date BETWEEN DATE_ADD('$date', INTERVAL - 3 MONTH) AND '$date'";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function casesForDate($date)
    {
        $sql = "SELECT SUM(confirmed_today),sum(death_today),SUM(recovered_today),date from cases where date='2021-02-20'";
    }




    // za site drzavi
    public function allCountriesTotals()
    {
        $this->connect();
        $sql = "SELECT  cases.country_id,countries.name,cases.confirmed, cases.active,cases.recovered,cases.deaths,cases.date FROM cases 
        JOIN countries ON cases.country_id=countries.id
         JOIN (SELECT country_id, max(date) as Date FROM cases GROUP BY country_id) as a 
         ON cases.country_id = a.country_id and cases.date = a.date ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    // //////////////////////////////// za konkretna drzava
    //   data za edna drzava
    public function countryTotal($id)
    {
        $this->connect();
        $sql = "SELECT * FROM cases
        JOIN countries ON cases.country_id=countries.id
         WHERE country_id={$id} ORDER BY date DESC";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    // kolku cases na dnevno nivo denes
    public function countryToday($id)
    {
        $sql = "SELECT date,confirmed_today as confirmed,recovered_today as recovered,death_today as deaths FROM cases WHERE country_id=$id ORDER BY date DESC ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    // kolku cases ima za sekoj mesec za drzava
    public function countryMonth($id)
    {
        $date = date('Y-m-d');
        $this->connect();
        $sql = "SELECT SUM(confirmed_today)as confirmed ,
        SUM(recovered_today) as recovered,
        SUM(death_today)as deaths
         FROM cases WHERE date BETWEEN DATE_ADD('$date', INTERVAL - 1 MONTH) AND '$date' AND country_id=$id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }
    // kolku cases ima na 3 meseci
    public function countryThreeMonths($id)
    {
        $date = date('Y-m-d');
        $sql = "SELECT SUM(confirmed_today)as confirmed ,
        SUM(recovered_today) as recovered,
        SUM(death_today)as deaths
         FROM cases WHERE date BETWEEN DATE_ADD('{$date}', INTERVAL - 3 MONTH) AND '{$date}' AND country_id=$id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function countryForDate($id, $date)
    {
        $this->connect();

        $sql = "SELECT * FROM cases WHERE counrty_id=$id AND date=$date";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function countryDataMonthly($id)
    {
        $sql = "SELECT cases.date,countries.name as 'CountryName',sum(confirmed_today) AS 'ConfirmedCases', sum(death_today) AS 'DeathCases', sum(recovered_today) AS 'RecoveredCases', date FROM cases JOIN countries ON cases.country_id = countries.id WHERE country_id = :id GROUP BY YEAR(date),MONTH(date) ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(['id'=>$id]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
$queryObj = new Query();
