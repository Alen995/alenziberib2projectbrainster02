<?php

namespace App\Classes;



use App\Classes\Model;

class Cases extends Model
{
    protected $table = 'cases';

    private function insert(array $newCase)
    {
        $this->connect();
        $sql = "INSERT INTO cases(country_id, confirmed, deaths, recovered,active, date, confirmed_today, death_today, recovered_today)VALUES({$newCase['countryId']},{$newCase['confirmed']},{$newCase['deaths']},{$newCase['recovered']},{$newCase['active']},'{$newCase['date']}',{$newCase['confirmedToday']},{$newCase['deathsToday']},{$newCase['recoveredToday']})";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
    }
    private function dateExists($date, $countryId)
    {
        $this->connect();

        $sql = "SELECT * FROM cases WHERE date ='$date' AND country_id=$countryId";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return  $data;
    }


    public function seedTable()
    {

        $this->connect();

        $countries = $this->selectAll();

        foreach ($countries as $country) {

            $handle = curl_init();
            $url = "https://api.covid19api.com/total/dayone/country/{$country->slug}";
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($handle);
            curl_close($handle);
            $cases = json_decode($result, true);
            $i = 1;
            if (is_null($cases)) {
                die('api failed');
            }
            foreach ($cases as $key => $case) {
                $caseDatearr = explode('T', $case['Date']);
                $caseDate = $caseDatearr[0];
                if ($i == 10) {
                    $i = 1;
                    set_time_limit(3000);
                    sleep(60);
                }
                $newCase = [
                    'countryId' => $country->id,
                    'confirmed' => $case['Confirmed'],
                    'deaths' => $case['Deaths'],
                    'recovered' => $case['Recovered'],
                    'active' => $case['Active'],
                    'date' =>   $caseDate,
                ];
                if ($key === 0) {
                    $newCase['confirmedToday'] =  $case['Confirmed'];
                    $newCase['deathsToday'] =  $case['Deaths'];
                    $newCase['recoveredToday'] =  $case['Recovered'];

                    $this->insert($newCase);
                } else if ($key > 0) {
                    $prevIndex = $cases[$key - 1];
                    $newCase['confirmedToday'] = $case['Confirmed'] - $prevIndex['Confirmed'];
                    $newCase['deathsToday'] =  $case['Deaths'] - $prevIndex['Deaths'];
                    $newCase['recoveredToday'] = $case['Recovered'] - $prevIndex['Recovered'];

                    $this->insert($newCase);
                }
                $i++;
            }
        }
    }
    public function updateTable()
    {
        $this->connect();
        //$date = Last date data for each country
        $data = $this->countriesTotalData();

        $currentDate = date('Y-m-d', strtotime("-1 days"));

        $j = 1;
        foreach ($data as $country) {

            $date = strtotime($country->date);
            $lastDateFromDb = date('Y-m-d', $date + 86400);
            if ($lastDateFromDb >= $currentDate) {
            } else {
                if ($j == 10) {
                    $j = 1;
                    set_time_limit(20000);
                    sleep(60);
                    echo "time out <br>";
                }

                $handle = curl_init();
                $url = "https://api.covid19api.com/total/country/$country->slug?from=$lastDateFromDb&to=$currentDate";
                curl_setopt($handle, CURLOPT_URL, $url);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($handle);
                curl_close($handle);

                $newCases = json_decode($result, true);
                if (!$newCases) { 
            return "error";
                }
                foreach ($newCases as $key => $case) {
                    $caseDatearr = explode('T', $case['Date']);
                    $caseDate = $caseDatearr[0];

                    if ($this->dateExists($caseDate, $country->id) == false) {
                        $newCase = [
                            'countryId' => $country->id,
                            'confirmed' => $case['Confirmed'],
                            'deaths' => $case['Deaths'],
                            'recovered' => $case['Recovered'],
                            'active' => $case['Active'],
                            'date' => $caseDate,
                        ];

                        if ($key == 0) {
                            echo " if key =0 <br>";
                            $newCase['confirmedToday'] = $case['Confirmed']  - (int)$country->confirmed;
                            $newCase['deathsToday'] = $case['Deaths'] -  (int)$country->deaths;
                            $newCase['recoveredToday'] = $case['Recovered'] -  (int)$country->recovered;

                            $this->insert($newCase);

                            set_time_limit(1000000000000);
                            sleep(1);
                        } else if ($key > 0) {

                            $prevIndex = $newCases[$key - 1];
                            $newCase['confirmedToday'] = $case['Confirmed'] - $prevIndex['Confirmed'];
                            $newCase['deathsToday'] = $case['Deaths'] - $prevIndex['Deaths'];
                            $newCase['recoveredToday'] = $case['Recovered'] - $prevIndex['Recovered'];

                            $this->insert($newCase);
                            set_time_limit(1000000000000);
                            sleep(1);
                        }
                    } else {
                        echo "<br> data  exist <br>";
                    }
                }
            }
            $j++;
        }
    }
}

$caseObj = new Cases();
