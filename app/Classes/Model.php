<?php

namespace App\Classes;



use App\Database\Connection;

class  Model extends Connection
{
    protected $table;
  

    public function select($table)
    {
        $this->connect();

        $sql = "SELECT * FROM $table where hasdata='true'";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);

        return $data;
    }

    public function selectAll()
    {
        $this->connect();

        $sql = "SELECT * FROM $this->table";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);

        return $data;
    }


    public function selectWhereId($id)
    {
        $this->connect();

        $sql = "SELECT * FROM $this->table WHERE id=:id";
        $stmt = $this->pdo->prepare($sql);
        // $stmt->bindParam(":param1", $param1);

        $stmt->bindParam(":id", $id);

        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_OBJ);
        return $data;
    }
    public function countriesTotalData()
    {
        $this->connect();

        $sql = "SELECT  * FROM cases 
        JOIN countries ON cases.country_id=countries.id
         JOIN (SELECT country_id, max(date) as Date FROM cases GROUP BY country_id) as a 
         ON cases.country_id = a.country_id and cases.date = a.date";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }
}
$modelObj = new Model();


// countries.id,countries.slug,cases.date,cases.confirmed ,cases.deaths,cases.recovered


// SELECT COUNT(country_id),country_id FROM `cases` GROUP BY country_id;