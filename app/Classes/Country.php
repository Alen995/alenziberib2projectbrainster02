<?php

namespace App\Classes;



use App\Classes\Model;

class Country extends Model
{
    protected $table = 'countries';

    private function seedTable()
    {
        $this->connect();
        $handle = curl_init();
        $url = "https://api.covid19api.com/countries";
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($handle);
        curl_close($handle);
        $countries = json_decode($result, true);
        $sql = "INSERT INTO countries(name, slug,hasdata) VALUES (:name, :slug,:hasdata)";
        $stmt = $this->pdo->prepare($sql); //PDOStatement
        $i = 1;
        foreach ($countries as $country) {
            if ($i == 10) {
                $i = 1;
                echo "sleep ";
                set_time_limit(3000);
                sleep(60);
            }
            $handle = curl_init();
            $url = "https://api.covid19api.com/total/dayone/country/{$country['Slug']}";
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($handle);
            curl_close($handle);
            $cases = json_decode($result, true);
            if (empty($cases)) {
                $hasdata = 'false';
            } else {
                $hasdata = 'true';
            }
            $name = $country['Country'];
            $slug = $country['Slug'];
            $stmt->bindParam(":name", $name);
            $stmt->bindParam(":slug", $slug);
            $stmt->bindParam(":hasdata", $hasdata);
            $stmt->execute();
        }
        $i++;
    }
}
$countryObj = new Country();



