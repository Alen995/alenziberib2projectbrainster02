<?php 

namespace App\Classes;

use App\Classes\Model;


class User extends Model
{
    public function selectAllUsers()
    {
        $this->connect();
        $sql = 'SELECT * FROM admin';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $row;
    }

    public function checkUser($username, $password)
    {
        $this->connect();
        $users = $this->selectAllUsers();
        foreach ($users as $user) {
            if ($user->username ==$username && $user->password == $password) {
                session_start();
                $_SESSION['user'] = $username;

                header('Location: admin.php');
                
            } else {
                header('Location: login.php?error=true');
            }
        }
    }
}
$user=new User();