<nav class="navbar navbar-dark bg-dark ">
    <a class="navbar-brand ml-auto d-flex align-items-center " href="index.php">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Coronavirus_icon.svg/1024px-Coronavirus_icon.svg.png" width="70px" height="70px" alt="">
        <span class="h2 nav-btn">Covid Tacker</span>
    </a>

    <?php 

    if (isset($_SESSION['user'])) { ?>
        <a href="logout.php" class="ml-auto "> LOGOUT</a>
    <?php } else{
        echo "   <a href='login.php' class='ml-auto '> LOGIN</a>";
    } ?>
 
</nav>